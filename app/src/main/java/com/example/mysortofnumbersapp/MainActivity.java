package com.example.mysortofnumbersapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button buttonAddToList, buttonClearList;
    EditText editTextNumber;
    ListView listViewNumbers;

    ArrayList<Integer> numberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberList = new ArrayList<>();

        buttonAddToList = findViewById(R.id.buttonAddToList);
        buttonClearList = findViewById(R.id.buttonClearList);

        editTextNumber = findViewById(R.id.editTextNumber);

        listViewNumbers = findViewById(R.id.listViewNumbers);

        buttonAddToList.setOnClickListener(buttonAddToListOnClick);
        buttonClearList.setOnClickListener(buttonClearListOnClick);
    }

    View.OnClickListener buttonAddToListOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            int number;

            number = Integer.parseInt(editTextNumber.getText().toString());

            numberList.add(number);

            SortNumbersByOrder();

            RefreshListViewNumbers();
        }
    };

    View.OnClickListener buttonClearListOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            numberList.clear();
            RefreshListViewNumbers();
        }
    };


    void RefreshListViewNumbers()
    {
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1, numberList);

        listViewNumbers.setAdapter(adapter);
    }

    void SortNumbersByOrder()
    {
        int temp;
        boolean sort;
        int round = 0;

        do
        {
            sort = true;

            for (int i = 0; i < numberList.size()-1-round; i++)
            {
                if(numberList.get(i)>numberList.get(i+1))
                {
                    temp = numberList.get(i);
                    numberList.set(i, numberList.get(i+1));
                    numberList.set(i+1, temp);

                    sort = false;
                }
            }

            round++;

        }
        while (sort==false);
    }
}
